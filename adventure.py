import os
from sys import platform
import rooms
import items
import player
import inputHandler
from game import Game
from datetime import datetime

if platform == "win32":
    mySystem = "windows"
else:
    mySystem = "other"


output = ""
game = ""
player = player.Player();

def clearScreen( ):
    global mySystem
    if mySystem == "windows":
        os.system('cls')
    else:
        os.system('clear')

#Begins the game and creates all the objects in the world at startup.
def gameSetup( ):
    global output
    global game
    global player

    openingMessage = """

You are a burglar and have just broken into the house of a wealthy mark.
You have entered through the front door (south) and will exit the same way.
If you leave before you have taken all the valuables, you lose.
If you do not leave before time runs out (30 minutes), the police will arive and you lose.
If you are injured too much, you will lose.
Type "help" if you need assistance.

Are you ready? (Press Enter)
"""

    input(openingMessage)
    
    currentRoom = rooms.LivingRoom();
    roomList = {}    
    masterInventory = {}

    livingRoom = currentRoom;
    office = rooms.Office();
    bedroom = rooms.Bedroom();
    stairwell = rooms.Stairwell();
    kitchen = rooms.Kitchen();
    mainHall = rooms.MainHall();
    closet = rooms.Closet();
    upperHall = rooms.UpperHall();
    guestRoom = rooms.GuestRoom();
    utilityRoom = rooms.UtilityRoom();
    gameRoom = rooms.GameRoom();
    bathroom = rooms.Bathroom();
    guestBath = rooms.GuestBath();
    outside = rooms.Outside();
    
    roomList.update({'LivingRoom' : livingRoom})
    roomList.update({'Office' : office})
    roomList.update({'Bedroom' : bedroom})
    roomList.update({'Stairwell' : stairwell})
    roomList.update({'Kitchen' : kitchen})
    roomList.update({'MainHall' : mainHall})
    roomList.update({'Closet' : closet})
    roomList.update({'UpperHall' : upperHall})
    roomList.update({'GuestRoom' : guestRoom})
    roomList.update({'UtilityRoom' : utilityRoom})
    roomList.update({'GameRoom' : gameRoom})
    roomList.update({'Bathroom' : bathroom})
    roomList.update({'GuestBath' : guestBath})
    roomList.update({'Outside' : outside})

    #add all items at room level to the master inventory.
    for i in list(currentRoom.inventory):
        masterInventory.update({i : currentRoom.inventory[i]})

    #add all sub-items, one level below room level, to the master inventory.
    for a in list(masterInventory):
        if len(masterInventory[a].inventory) > 0:
            for x in list(masterInventory[a].inventory):
                masterInventory.update({x : masterInventory[a].inventory[x]})

    #Add user's inventory to master inventory
    for p in list(player.inventory):
        masterInventory.update({p : player.inventory[p]})

    output = currentRoom.description

    #create master game object
    game = Game(currentRoom, player, masterInventory, roomList, datetime.now())

def inputReceiver(game):
    global output
    while game.gameover == False:

        print (output)
		
        userInput = str.lower(input("\r\n Enter command: "))
        clearScreen();
        inputWords = userInput.split()
        numWords = len(inputWords)
        game.inputWords = inputWords
        options = {0 : inputHandler.noInput,
						1 : inputHandler.inputOneWord,
						2 : inputHandler.inputTwoWords,
						3 : inputHandler.inputThreeWords,
        }
        if numWords < 4:
            output = options[numWords](game);
        else:
            output = "Too much input. Try again."

        if game.player.hp == 0:
            output = game.endGame()

        
        #check for time limit
        if game.getGameTime() > game.timeLimit:
            output = game.endGame();
    
    print (output)
    userInput = str.lower(input("\r\n Press ENTER to end the game: ")) 
   
clearScreen();
gameSetup();
inputReceiver(game);