import items
from game import Game

'''

TODO:  Create outside room!

'''
class LivingRoom:
    'Room player starts the game in.'
    
    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']

        self.inventory = {'table' : items.CoffeeTable(),
    				'couch' : items.Couch(),
    				'chair' : items.Chair(),
                }
        self.exits = {'south' : 'Outside',
                        'east' : 'Kitchen',
                        'north' : 'MainHall',
                        'up' : 'UpperHall',
            }
        self.name = "livning room"
        self.description = """
The living room.  
There is a coffee table, a couch, and a chair.  
Pretty basic for a rich guy's house.  
There is a staircase leading up.
"""
        self.locked = False

class Kitchen:

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.exits = {'west' : 'LivingRoom',
                        }
    
        self.inventory = {'sink' : items.KitchenSink(),
                    'refrigerator' : items.Fridge(),
                    'cabinet' : items.Cabinet(),
            }

        self.name = "kitchen"
        self.description = """
The kitchen.  
Just as plain as the living room.  
You see a sink, a refrigerator, and a china cabinet.
"""
        self.locked = False

class UpperHall:

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.exits = {'down' : 'LivingRoom',
                    'west' : 'GuestRoom',
                    'east' : 'GuestBath',
        }

        self.inventory = {}

        self.name = "upper hall"
        self.description = """
A small hall on the top floor. 
Nothing exciting here.
"""
        self.locked = False
   
class GuestRoom:
    'Room south of start room.'
    
    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.exits = {'east' : 'UpperHall',
                }

        self.inventory = { 'bed' : items.TwinBed(),
                            'lamp' : items.Lamp(),
                        }
    
        self.name = "guest room"
        self.description = """
This is the guest room.  
The only items in the room are a single twin bed against the wall with a lamp beside it.
"""
        self.locked = False
       
class GuestBath:
    

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.exits = {'west' : 'UpperHall',
                }

        self.inventory = {'toilet' : items.GuestToilet(),
                    'sink' : items.GuestSink(),
                    'shower' : items.GuestShower(),
                }
    
        self.name = "guest bath"
        self.description = """
A full bathroom for guests complete with a shower stall.
"""
        self.locked = False
    
class MainHall:
    
    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.exits = {'south' : 'LivingRoom',
                'north' : 'Office',
                'west' : 'Bedroom',
                'east' : 'Bathroom',
                'down' : 'Stairwell',
            }

        self.inventory = {}
    
        self.name = "main hall"
        self.description = """"
The main hall here has several doors all around and a staircase leading down.
"""
        self.locked = False
    
class Bedroom:
    
    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.exits = {'east' : 'MainHall',
                }

        self.inventory = {'bed' : items.KingBed(),
                    'dresser' : items.Dresser(),
                    'dog' : items.Dog(),
                    'jewelery box' : items.JeweleryBox(),
                }
    
        self.name = "bedroom"
        self.description = """
This is the master bedroom.  
You see a king sized bed, a dresser, a jewelery box, and a large dog.
"""
        self.locked = False
   
class Office:
    
    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.exits = {'south' : 'MainHall',
                'west' : 'Closet',
                }

        self.inventory = {'desk' : items.Desk(),
                    'computer' : items.Computer(),
                    'lockbox' : items.LockBox(),
                }
    
        self.locked = True
        self.name = "office"
        self.description = """
This is a small office.  
You see a desk with a computer.  
There is a small closet to the west.
"""
    
    def unlock(self, game):
        if 'office key' in game.player.inventory:
            self.locked = False
            return True
        else:
            return False
    
        
class Closet:
    
    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.exits = {'east' : 'Office',
                    }
        self.inventory = {'coats' : items.Coats(),
                        'safe' : items.Safe(),
                    }
        self.name = "closet"
        self.description = """
This walk in closet is bigger than it looks.  
It is filled with coats.
"""
        self.locked = False

class Bathroom:
    
    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.exits = {'west' : 'MainHall',
                    }

        self.inventory = {'mirror' : items.Mirror(),
                    'toilet' : items.Toilet(),
                    'shower' : items.Shower(),
                    'sink' : items.BathSink(),
                }
    
        self.name = "bathroom"
        self.description = """
For a full bathroom, this is kind of small.  
How can anyone even see themselves in that mirror?
"""
        self.locked = False

class Stairwell:
    
    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.exits = {'up' : 'MainHall',
                'west' : 'GameRoom',
                'east' : 'UtilityRoom',
                }

        self.inventory = {}
    
        self.name = "stairwell"
        self.description = """
At the bottom of the stairwell you can only go west or east.
"""
        self.locked = False

class GameRoom:
    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.exits = {'east' : 'Stairwell',
                    }

        self.inventory = {'poker table' : items.PokerTable(),
                    'dart board' : items.DartBoard(),
                    'screws' : items.Screws(self),
                }
    
        self.name = "game room"
        self.description = """
This is the game room.  
There is a poker table and a dart board along the far wall.  
Who hangs a painting next to a dart board?
"""
        self.locked = True

    def unlock(self, game):
        riddleAnswer = str.lower(input("\r\n To enter this room, answer this riddle: What gets wetter as it dries?: "))
        if riddleAnswer == "towel" or riddleAnswer == "a towel":
            self.locked = False
            return True

        return False
        

class UtilityRoom:
    
    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.exits = {'west' : 'Stairwell',
                    }

        self.inventory = {'washer' : items.Washer(),
                    'dryer' : items.Dryer(),
                }
    
        self.name = "utility room"
        self.description = """
This must be the utility room.  
Must be nice to have your own washer and dryer.
"""
        self.locked = False

class Outside:

    def __init__(self):
        self.inventory = {}
        self.actionWords = []
        self.exits = {}
        self.description = "You are outside."
        self.name = "outside"
        self.locked = False
        