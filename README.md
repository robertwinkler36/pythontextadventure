# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository contains the code for a text adventure game written in Python.
The game was written as a way to begin learning the Python language.

### How do I get set up? ###

To get started, unpackage all the files of the repository into a directory on your machine and run adventure.py

If you want to use this engine to make the game different, keep these notes in mind:

    1) Create rooms using the patterns in rooms.py
    2) Create items using the patterns in items.py
    3) Items stored in other items need to be passed their parent item at __init__ time.
    4) Alter the Game object variable to suit your needs (ie: win scenario, time limit, etc)
    5) Alter the game setup method in adventure.py to include the new rooms and other details.

### Contribution guidelines ###

None at this time.

### Who do I talk to? ###

Repo owner: robertwinkler36@gmail.com