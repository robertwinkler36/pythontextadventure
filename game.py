from datetime import datetime

class Game:
    inputWords = ""

    def __init__(self, location, player, masterInventory, roomList, startTime):
        self.location = location
        self.player = player
        self.walkingWords = ['go', 'walk', 'run']
        self.actionWords = ['use', 'take', 'examine', 'inspect', 'move', 'search', 'give']
        #masterInventory holds all items in the current room and the player's inventory.
        #This allows for a central place to check for items to be interacted with.
        self.masterInventory = masterInventory
        self.roomList = roomList
        self.startTime = startTime
        self.timeLimit = 30 #time limit for game in minutes.
        self.gameover = False

    #Returns how long the game has been going on.
    def getGameTime(self):
        currentTime = datetime.now()
        timeDiff = currentTime - self.startTime
        return int(round(timeDiff.total_seconds() / 60))

    #Ends the game and gives a final message based on end game conditions.
    def endGame(self):
        allTreasures = False
        timeRemaining = True
        self.gameover = True

        if self.getGameTime() > self.timeLimit: 
            timeRemaining = False

        if self.player.hp == 0:
            return "You lost too much health.  You lose!"

        if 'rings' in self.player.inventory and 'painting' in self.player.inventory and 'jewels' in self.player.inventory and 'cash' in self.player.inventory and 'silverware' in self.player.inventory:
            allTreasures = True

        if timeRemaining == False:
            return "You did not exit the house with the valuables in time.  You find the police waiting for you.  You lose."
        elif allTreasures == False:
            return "You did not find all the valuables.  You lose."        
        else: 
            return "You stole all the valuables and made it out in time.  You win!"
