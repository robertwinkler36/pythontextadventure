from game import Game
from datetime import datetime

def noInput( game ):
	return "No input. Try again."

def inputOneWord( game ):
    inventoryStr = ""

    action = game.inputWords[0]
    directions =  game.location.exits
    if action in directions:
        output = enterRoom(game, action);
    elif action == "room":
        output = game.location.description
    elif action == "quit":
        game.gameover = True
        output = "gameover"
    elif action == "inventory":
        for i in game.player.inventory:
            inventoryStr += i + ", "
            inventoryStr = inventoryStr[:-2]
        output = inventoryStr
    elif action == "inventorymaster":
        for i in game.masterInventory:
            inventoryStr += i
        output = inventoryStr
    elif action == "help":
        #display help message
        output = help();
    else:
        output = "You cannot do that here.  Try again."
    return output

def inputTwoWords( game ):
    actionWords = {'inspect' : search,
                'examine' : search,
                'search' : search,
                'take' : take,
                'move' : move,
                'use' : use,
                'give' : use,
            }
    action = game.inputWords[0]
    target = game.inputWords[1]
    output = "You cannot do that here"

    #Identify target object
    #Access function asocciated with that word.
    if action in game.walkingWords:
        if target in game.location.exits:                
                output = enterRoom(game, target);
    elif action in game.actionWords:
        if target in game.masterInventory:
            if action in game.masterInventory[target].actionWords:
                output = actionWords[action](game, target);
        elif target == "room" or target == "here" or target == game.location.name:
            if action in game.location.actionWords:
                output = actionWords[action](game, target);
        elif target == "self":
            if action in game.player.actionWords:
                output = actionWords[action](game, target);
    return output

def inputThreeWords( game ):
    target = game.inputWords[1] + " " + game.inputWords[2]
    game.inputWords[1] = target
    return inputTwoWords(game)

def enterRoom(game, direction):
    nextRoomName = game.location.exits[direction]
    #Condition for locked room
    if game.roomList[nextRoomName].locked == True:
        if game.roomList[nextRoomName].unlock(game) == False:
            return "That way is locked."
        else:
            game.location = game.roomList[nextRoomName]
            updateMasterInventory(game);
            return game.location.description

    #Condition for exiting the house.
    elif nextRoomName == "Outside":
        #trigger end game
        return game.endGame();

    #Condition for entering any room normally
    else:
        game.location = game.roomList[nextRoomName]
        updateMasterInventory(game);
        return game.location.description
        
def updateMasterInventory(game):
    #clear out master inventory
    game.masterInventory.clear()

    #Add every item at room level to the master inventory
    for i in list(game.location.inventory):
        game.masterInventory.update({i : game.location.inventory[i]})

    #Add sub-items, one level below room level, in the room to the master inventory
    for a in list(game.masterInventory):
        if len(game.masterInventory[a].inventory) > 0:
            for x in list(game.masterInventory[a].inventory):
                game.masterInventory.update({x : game.masterInventory[a].inventory[x]})

    #Add user's inventory to master inventory
    for p in list(game.player.inventory):
        game.masterInventory.update({p : game.player.inventory[p]})

#Using the search action word
def search(game, target):
    searchStr = "\r\n You find: "

    if target in game.masterInventory:
        if game.masterInventory[target].locked == True:
            unlockMessage = game.masterInventory[target].unlock(game);
            return unlockMessage
        if len(game.masterInventory[target].inventory) > 0:
            for i in game.masterInventory[target].inventory:                
                searchStr += game.masterInventory[target].inventory[i].name + ", "
        else:
            searchStr += " Nothing. "
        
        searchStr = searchStr[:-2]
        return game.masterInventory[target].description + searchStr
    elif target == "room" or target == game.location.name or target == "here":
        for a in game.location.inventory:                
                searchStr += game.location.inventory[a].name + ", "
        searchStr = searchStr[:-2]
        return game.location.description + searchStr
    elif target == "self" or target == "me":
        return game.player.description + " " + str(game.player.hp) + "HP.  " + str(game.getGameTime()) + " minutes passed.  " 
#using the Take action word
def take( game, target):
        #NOTE:::: We may have to take the item out of the master inventory and then put it
        #back in if the memory does not handle correctly.
        #add item to player's inventory
        if game.masterInventory[target].parent.locked == False:
            game.player.inventory.update({game.masterInventory[target].name : game.masterInventory[target]})
            #remove item from whereever it was
            del game.masterInventory[target].parent.inventory[target]
            return "Picked up " + target
        else:
            return "Ah ah ah...no cheating."

#using the Move action word
def move(game, target):
    return game.masterInventory[target].move();

#using the Use action word
def use(game, target):
    return game.masterInventory[target].use(game);

#using the Help action word
def help():
    helpStr = """
The following is a list of commands you may find useful:

    search, examine, inspect {target} : Get details about {target}
    
    room : Get description of your current location

    quit : Ends the game.

    use {target} : Makes use of {target}

    take {target} : Puts {target} in your personal inventory

    move {target} : Moves {target}

    inventory : Lists items you are carrying

    {direction} : Moves you in that direction

    HINTS:

        1) You only have to have a key in your possesion to unlock what it goes to.

        2) Not all paths are apparent or spelled out in a room's description. 

"""

    return helpStr