class Couch:
    inventory = {}
    
    def __init__(self):
        self.name = "couch"  
        self.officeKey = OfficeKey(self)      
        self.inventory.update({'office key' : self.officeKey})
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "Leather sofa.  Very nice.  "
        self.name = "couch"
        self.locked = False
#
class Chair:
    inventory = {}

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "Overstuffed and leather."
        self.name = "chair"
        self.locked = False

#
class Lamp:
    inventory = {}

    def __init__(self):
        self.lockboxkey = LockBoxKey(self)      
        self.inventory.update({'lockbox key' : self.lockboxkey})
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "Basic bedside lamp. "
        self.name = "lamp"
        self.locked = False
#
class TwinBed:
    inventory = {}

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "Bed for a single person.  Looks comfortable."
        self.name = "bed"
        self.locked = False
#
class Fridge:
    inventory = {}

    def __init__(self):
        self.meat = Meat(self)      
        self.inventory.update({'meat' : self.meat})
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "Pretty Bare.  Someone needs to go shopping."
        self.name = "refrigerator"
        self.locked = False
#
class KitchenSink:
    inventory = {}

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "Nothing to see here."
        self.name = "sink"
        self.locked = False

#
class Cabinet:
    inventory = {}

    def __init__(self):
        self.silverware = Silverware(self)      
        self.inventory.update({'silverware' : self.silverware})
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "The drawers are locked.  Wonder what is inside."
        self.name = "cabinet"
        self.locked = True

    def unlock(self, game):
        if 'china key' in game.player.inventory:
            self.locked = False 
            self.description = "Not much in here. "
            message = "You unlocked it."
        else:
            message = "It is locked!"

        return message
#
class CoffeeTable:
    inventory = {}

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "Using coasters would keep water rings from forming."
        self.name = "table"
        self.locked = False
#
class GuestToilet:
    inventory = {}

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "Opening up the tank you see a set of numbers: 12345."
        self.name = "toilet"
        self.locked = False

    
#
class GuestSink:
    inventory = {}

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "Always a good idea to wash up."
        self.name = "sink"
        self.locked = False
#
class GuestShower:
    inventory = {}

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "Plain old shower stall."
        self.name = "shower"
        self.locked = False
#
class KingBed:
    inventory = {}

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "That mattress is as soft as a marshmallow."
        self.name = "bed"
        self.locked = True

    
    def unlock(self, game):
        if self.locked == False:
            self.values['status'] = True    
            message = self.description
        else:
            game.player.hp -= 20
            message = "The dog bit you!  You lost health!"

        return message
#
class Dresser:
    inventory = {}

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "Solid oak dresser."
        self.name = "dresser"
        self.locked = True

    def unlock(self, game):
        if self.locked == False:
            self.values['status'] = True    
            message = self.description
        else:
            game.player.hp -= 20
            message = "The dog bit you!  You lost health!"

        return message

#Might be able to use the locked variable for using the meat on the dog
class Dog:
    inventory = {}

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "He does not look friendly."
        self.name = "dog"
        self.locked = True

    
    def unlock(self, game):
        if self.locked == False:
            self.values['status'] = True    
            message = self.description
        else:
            game.player.hp -= 20
            message = "The dog bit you!  You lost health!"

        return message
#
class Desk:
    inventory = {}

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "This guys likes to work from home a lot."
        self.name = "desk"
        self.locked = False
#
class Computer:
    inventory = {}

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "The power cord has been cut.  Oh well."
        self.name = "computer"
        self.locked = False
#
class Coats:
    inventory = {}

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search', 'move']
        self.description = "Who needs this many winter coats?"
        self.name = "coats"
        self.locked = False

    def move(self):
        return "You find a safe behind the coats."

class Safe:
    inventory = {}

    def __init__(self):
        self.jewels = Jewels(self)      
        self.inventory.update({'jewels' : self.jewels})
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "Sturdy safe.  Needs a combination to open."
        self.name = "safe"
        self.locked = True

    def unlock(self, game):
        combinationInput = str.lower(input("\r\n What is the combination: "))
        if combinationInput == "12345":
            self.locked = False
            message = "You opened the safe."
        else:
            message = "It is locked!"
        return message;
#
class Mirror:
    inventory = {}

    def __init__(self):
        self.screwdriver = Screwdriver(self)
        self.inventory.update({'screwdriver' : self.screwdriver})
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "Standard bathroom medicine mirror."  
        self.name = "mirror"
        self.locked = False
#
class Toilet:
    inventory = {}

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "Flush..."
        self.name = "toilet"
        self.locked = False
#
class Shower:
    inventory = {}

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "A little Scrubbing Bubbles would take care of that ring."
        self.name = "shower"
        self.locked = False
#
class BathSink:
    inventory = {}

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "Always a good idea to wash up."
        self.name = "sink"
        self.locked = False
#
class Screwdriver:
    inventory = {}

    def __init__(self, parent):
        self.actionWords = ['inspect', 'examine', 'search', 'take', 'use']
        self.description = "A simple tool."
        self.name = "screwdriver"
        self.parent = parent
        self.locked = False

    def use(self, game):
        if 'screws' in game.location.inventory:
            game.location.inventory['screws'].locked = False
            game.location.inventory['screws'].inventory['painting'].locked = False
            game.location.inventory['screws'].inventory['painting'].description = "This belongs in a museum."
            return 'Unscrewed the screws'
        else:   
            return 'Not a good time for this.'
#
class PokerTable:
    inventory = {}

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "Hit me!."
        self.name = "poker table"
        self.locked = False
#
class DartBoard:
    inventory = {}

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "Bullseye!."
        self.name = "dart board"
        self.locked = False

#Might be able to leverage the locked variable for the action of unscrewing the painting from the wall.
class Painting:
    inventory = {}

    def __init__(self, parent):
        self.actionWords = ['inspect', 'examine', 'search', 'take']
        self.description = "This belongs in a museum. Too bad it is screwed to the wall."
        self.name = "painting"
        self.parent = parent
        self.locked = True

    def unlock(self, game):
        if self.locked == False:
            self.description = "This belongs in a museum."

        return self.description
#
class Washer:
    inventory = {}

    def __init__(self):
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "Where's the Maytag Man?"
        self.name = "washer"
        self.locked = False
#
class Dryer:
    inventory = {}

    def __init__(self):
        self.chinaKey = ChinaKey(self)      
        self.inventory.update({'china key' : self.chinaKey})
        self.actionWords = ['inspect', 'examine', 'search']
        self.description = "It's electric!  Boogy woogy woogy."
        self.name = "dryer"
        self.locked = False

#
class OfficeKey:
    inventory = {}

    def __init__(self, parent):
        self.actionWords = ['inspect', 'examine', 'search', 'take']
        self.parent = parent
        self.description = "The key appears to fit a door."
        self.name = "office key"  
        self.locked = False  

#
class Meat:
    inventory = {}

    def __init__(self, parent):
        self.parent = parent
        self.actionWords = ['inspect', 'examine', 'search', 'take', 'give', 'use']
        self.description = "Fresh piece of meat.  Looks appetizing."
        self.name = "meat"
        self.locked = False

    def use(self, game):
        if 'dog' in game.location.inventory:
            game.location.inventory['dog'].locked = False
            game.location.inventory['bed'].locked = False
            game.location.inventory['dresser'].locked = False
            game.location.inventory['jewelery box'].locked = False
            del game.player.inventory['meat']
            return 'Fed meat to the dog.'
        else:   
            return 'Not a good time for this.'
#
class LockBoxKey:
    inventory = {}

    def __init__(self, parent):
        self.parent = parent
        self.actionWords = ['inspect', 'examine', 'search', 'take']
        self.description = "Small key for a lock box."
        self.name = "lockbox key"
        self.locked = False

#
class ChinaKey:
    inventory = {}

    def __init__(self, parent):
        self.parent = parent
        self.actionWords = ['inspect', 'examine', 'search', 'take']
        self.description = "The key is marked 'Cabinet'."
        self.name = "china key"
        self.locked = False

class Rings:
    inventory = {}

    def __init__(self, parent):
        self.parent = parent
        self.actionWords = ['inspect', 'examine', 'search', 'take']
        self.description = "Certainly not costume jewelery."
        self.name = "rings"
        self.locked = False
#
class Cash:
    inventory = {}

    def __init__(self, parent):
        self.parent = parent
        self.actionWords = ['inspect', 'examine', 'search', 'take']
        self.description = "There must be $20,000 here."
        self.name = "cash"
        self.locked = False
#
class Silverware:
    inventory = {}

    def __init__(self, parent):
        self.parent = parent
        self.actionWords = ['inspect', 'examine', 'search', 'take']
        self.description = "Flatware made of real silver."
        self.name = "silverware"
        self.locked = False

#
class Jewels:
    inventory = {}

    def __init__(self, parent):
        self.parent = parent
        self.actionWords = ['inspect', 'examine', 'search', 'take']
        self.description = "Precious stones."
        self.name = "jewels"
        self.locked = False

class LockBox:
    inventory = {}

    def __init__(self):
        self.cash = Cash(self)
        self.inventory.update({'cash' : self.cash})
        self.actionWords = ['search', 'inspect', 'examine']
        self.description = "Needs a key to find what is protected inside."
        self.name =  "lockbox"
        self.locked = True

    def unlock(self, game):
        if 'lockbox key' in game.player.inventory:
            self.locked = False
            self.description = "Holy cow!  Look at all that!" 
            message = "You unlocked it!"
        else:
            message = "It is locked!"

        return message

class JeweleryBox:
    inventory = {}

    def __init__(self):
        self.rings = Rings(self)
        self.inventory.update({'rings' : self.rings})
        self.actionWords = ['search', 'examine', 'inspect']
        self.description = "If it was wound, it would play music."
        self.name = "jewelery box"
        self.locked = True

    def unlock(self, game):
        if self.locked == False:
            self.values['status'] = True    
            message = self.description
        else:
            hp = game.player.hp
            game.player.hp = hp - 20
            message = "The dog bit you!  You lost health!"

        return message

class Screws:
    inventory = {}

    def __init__(self, parent):
        self.painting = Painting(self)
        self.inventory.update({'painting' : self.painting})
        self.actionWords = ['search', 'examine', 'inspect']
        self.description = "Basic wood screws."
        self.name = "screws"
        self.locked = True

    def unlock(self, game):
        return self.description